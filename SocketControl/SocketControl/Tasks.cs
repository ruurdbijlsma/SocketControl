﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace SocketControl
{
    class Tasks
    {
        public static IEnumerable<String> List()
        {
            var tasks = Process.GetProcesses();
            var titles = tasks.Select(task => task.MainWindowTitle);
            return titles.Where(title => title != "");
        }

        public static void Kill(string title)
        {
            var tasks = Process.GetProcesses();
            Process taskToKill = tasks.First(task => task.MainWindowTitle == title);
            taskToKill.Kill();
        }

        public static void KillAll()
        {
            foreach (var task in Process.GetProcesses().Where(task => task.MainWindowTitle != ""))
            {
                task.Kill();
            }
        }
    }
}
