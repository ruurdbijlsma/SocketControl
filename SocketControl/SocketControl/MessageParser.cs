﻿using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Diagnostics;

namespace SocketControl
{
    class MessageParser
    {
        public static async Task<string> Parse(string text)
        {
            JObject json;
            if (!Utilities.TryParseJObject(text, out json))
                return invalidJson;

            JToken action;
            if (!json.TryGetValue("action", out action)) return string.Format(fieldMissing, "action");
            switch (((string)json["action"]).ToLower())
            {
                case "playpause":
                    return PlayPause(json);
                case "getvolume":
                    return GetVolume(json);
                case "setvolume":
                    return SetVolume(json);
                case "next":
                    return Next(json);
                case "previous":
                    return Previous(json);
                case "gettasks":
                    return GetTasks(json);
                case "killtask":
                    return KillTask(json);
                case "shutdown":
                    return ShutDown(json);
                case "command":
                    return Command(json);

                default:
                    return JsonConvert.SerializeObject(new
                    {
                        success = false,
                        code = 400,
                        error = "Action doesn't exist."
                    });
            }
        }

        private static string Command(JObject data)
        {
            JToken command;
            if (!data.TryGetValue("command", out command)) return string.Format(fieldMissing, "command");
            JToken arguments;
            if (!data.TryGetValue("arguments", out arguments)) return string.Format(fieldMissing, "arguments");

            Process.Start((string)command, (string)arguments);

            return success;
        }

        private static string ShutDown(JObject data)
        {
            Tasks.KillAll();

            Process.Start("shutdown", "/s /t 2");
            return success;
        }

        private static string GetTasks(JObject data)
        {
            return JsonConvert.SerializeObject(new
            {
                success = true,
                code = 200,
                tasks = Tasks.List()
            });
        }
        private static string KillTask(JObject data)
        {
            JToken task;
            if (!data.TryGetValue("task", out task)) return string.Format(fieldMissing, "task");

            Tasks.Kill((string)task);

            return success;
        }




        private static string PlayPause(JObject data)
        {
            KeyBoard.SendKeyPress(KeyBoard.KeyCode.MEDIA_PLAY_PAUSE);
            return success;
        }

        private static string Next(JObject data)
        {
            KeyBoard.SendKeyPress(KeyBoard.KeyCode.MEDIA_NEXT_TRACK);
            return success;
        }

        private static string Previous(JObject data)
        {
            KeyBoard.SendKeyPress(KeyBoard.KeyCode.MEDIA_PREV_TRACK);
            return success;
        }

        private static string SetVolume(JObject data)
        {
            JToken volume;
            if (!data.TryGetValue("volume", out volume)) return string.Format(fieldMissing, "volume");

            Audio.Volume = ((float)volume) / 100;

            return success;
        }

        private static string GetVolume(JObject data)
        {
            return JsonConvert.SerializeObject(new
            {
                success = true,
                code = 200,
                volume = Audio.Volume
            });
        }

        #region defaultMessages

        private static readonly string success = JsonConvert.SerializeObject(new
        {
            success = true,
            code = 200
        });

        private static readonly string notAuthorized = JsonConvert.SerializeObject(new
        {
            success = false,
            code = 400,
            error = "Token is not supplied or invalid."
        });

        private static readonly string invalidJson = JsonConvert.SerializeObject(new
        {
            success = false,
            code = 400,
            error = "JSON was invalid."
        });

        private static readonly string fieldMissing = JsonConvert.SerializeObject(new
        {
            success = false,
            code = 400,
            error = "field {0} was missing."
        });

        private static readonly string invalidIndex = JsonConvert.SerializeObject(new
        {
            success = false,
            code = 400,
            error = "Invalid index."
        });

        private static readonly string invalidParameters = JsonConvert.SerializeObject(new
        {
            success = false,
            code = 400,
            error = "Parameters are in wrong format."
        });

        #endregion
    }
}