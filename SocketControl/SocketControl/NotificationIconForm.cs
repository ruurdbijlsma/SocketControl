﻿using System;
using System.Drawing;
using System.Windows.Forms;
using vtortola.WebSockets;
using Newtonsoft.Json;
using Microsoft.Win32;

namespace SocketControl
{
    public partial class NotificationIconForm : Form
    {
        private NotifyIcon notifyIcon;
        RegistryKey rkApp = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

        public NotificationIconForm()
        {
            InitializeComponent();
            FormBorderStyle = FormBorderStyle.FixedToolWindow;
            ShowInTaskbar = false;
            StartPosition = FormStartPosition.Manual;
            Location = new Point(-2000, -2000);
            Size = new Size(1, 1);

            var menuItem = new MenuItem
            {
                Index = 0,
                Text = "Exit"
            };
            menuItem.Click += MenuItem_Click;

            var contextMenu = new ContextMenu();
            contextMenu.MenuItems.AddRange(
                new[] {menuItem}
            );

            notifyIcon = new NotifyIcon()
            {
                Icon = new Icon("favicon.ico"),
                ContextMenu = contextMenu,
                Text = "SocketControl",
                Visible = true
            };
            notifyIcon.DoubleClick += NotifyIcon_DoubleClick;
            
            rkApp.SetValue("SocketControl", Application.ExecutablePath);
        }

        public string NotifyIconText
        {
            set { notifyIcon.Text = value; }
            get { return notifyIcon.Text; }
        }

        private void NotifyIcon_DoubleClick(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
                WindowState = FormWindowState.Normal;

            Activate();
        }

        private void MenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}