﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NAudio.CoreAudioApi;

namespace SocketControl
{
    class Audio
    {
        private static MMDeviceCollection devices = new MMDeviceEnumerator().EnumerateAudioEndPoints(DataFlow.All, DeviceState.All);
        public static float Volume
        {
            set
            {
                foreach (var device in devices)
                {
                    if (device.State == DeviceState.Active)
                        device.AudioEndpointVolume.MasterVolumeLevelScalar = value;
                }
            }
            get
            {
                foreach (var device in devices)
                {
                    if (device.State == DeviceState.Active)
                        return device.AudioEndpointVolume.MasterVolumeLevelScalar;
                }
                return 0;
            }
        }
    }
}
