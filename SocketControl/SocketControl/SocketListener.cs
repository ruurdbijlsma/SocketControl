﻿using Microsoft.Win32;
using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using vtortola.WebSockets;
using vtortola.WebSockets.Rfc6455;

namespace SocketControl
{
    public class SocketListener
    {
        private const int Port = 912;
        private static readonly CancellationToken CancellationToken = new CancellationToken();
        private static WebSocketListener server;

        public static string Initialize()
        {
            IPAddress ip = Array.Find(Dns.GetHostEntry(Dns.GetHostName()).AddressList,
                a => a.AddressFamily == AddressFamily.InterNetwork);

            var serverOptions = new WebSocketListenerOptions();
            server = new WebSocketListener(new IPEndPoint(ip, Port), serverOptions);
            var rfc6455 = new WebSocketFactoryRfc6455(server);
            server.Standards.RegisterStandard(rfc6455);
            server.Start();

            var worker = new Thread(StartListening);
            worker.Start();
            return ip.ToString();
        }

        private static async void StartListening()
        {
            for (;;)
            {
                WebSocket client = await server.AcceptWebSocketAsync(CancellationToken);

                WebSocketMessageReadStream messageStream = await client.ReadMessageAsync(CancellationToken);

                if (messageStream == null || messageStream.MessageType != WebSocketMessageType.Text) continue;
                string result;

                using (var reader = new StreamReader(messageStream))
                {
                    result = reader.ReadToEnd();
                }
                var response = await MessageParser.Parse(result);

                SendResponse(response, client);
            }
        }

        private static async void SendResponse(string response, WebSocket client)
        {
            try
            {
                await client.WriteStringAsync(response, CancellationToken);
            }
            catch
            {
                Console.WriteLine("Connection closed.");
            }
        }
    }
}